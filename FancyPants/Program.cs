﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FancyPants
{
    class Program
    {
        static void Main(string[] args)
        {
            //Title
            Console.Title = Constants.FANCY_PANTS;
            //Enter Postive Integer
            Console.WriteLine(Constants.ENTER_POSITIVE_INTEGER);
            Console.WriteLine(Constants.DASH);

            //Low
            string readLow;
            do
            {
                readLow = Program.ReadValue(Constants.ENTER + " " + Constants.LOW);
            }
            while (!Validate(readLow));

            //High
            string readHigh;
            do
            {
                readHigh = Program.ReadValue(Constants.ENTER + " " + Constants.HIGH);
            }
            while (!Validate(readHigh));

            //A
            string readA;
            do
            {
                readA = Program.ReadValue(Constants.ENTER + " " + Constants.A);
            }
            while (!Validate(readA));

            //B
            string readB;
            do
            {
                readB = Program.ReadValue(Constants.ENTER + " " + Constants.B);
            }
            while (!Validate(readB));

            //Print & Display
            string output = string.Empty;
            Console.WriteLine(Constants.DASH);
            Console.WriteLine(Constants.OUTPUT);
            Console.WriteLine(Constants.DASH);

            //Convert to integer
            int low = Convert.ToInt32(readLow);
            int high = Convert.ToInt32(readHigh);
            int a = Convert.ToInt32(readA);
            int b = Convert.ToInt32(readB);

            //Add to List Collection
            var list = new List<int>
            {
                low,
                a,
                b,
                high
            };

            //Sort
            list.Sort();

            foreach (var number in list)
            {
                output = string.Empty;
                if (number % a == 0)
                    output = Constants.FANCY;
                if (number % b == 0)
                    output += Constants.PANTS;

                if (output == string.Empty)
                    Console.WriteLine(number);
                else
                    Console.WriteLine(output);
            }
            Console.ReadLine();
        }

        private static bool Validate(string inputValue)
        {
            //Validate for positive integer
            return uint.TryParse(inputValue, out uint ret);
        }

        private static string ReadValue(string placeHolder)
        {
            //Imput Message
            Console.Write("{0}: ", placeHolder);
            //Read Input
            return Console.ReadLine();
        }
    }

    //Constants
    static class Constants
    {
        public static readonly string LOW = "Low";
        public static readonly string HIGH = "High";
        public static readonly string A = "A";
        public static readonly string B = "B";
        public static readonly string ENTER = "Enter";
        public static readonly string OUTPUT = "Output";
        public static readonly string DASH = "------------------------------";
        public static readonly string FANCY = "Fancy";
        public static readonly string PANTS = "Pants";
        public static readonly string FANCY_PANTS = "FancyPants";
        public static readonly string ENTER_POSITIVE_INTEGER = "Please enter positive integers";
    }
}